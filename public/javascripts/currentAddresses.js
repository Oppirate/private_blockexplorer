function getCurrencyAddresses(selectedCurrency, currenciesC){
    // var currenciesC = cb;
    var currencySelected = selectedCurrency.value;
    var addressSelectionOptions = document.getElementById("addresses");
    var optionsLength = addressSelectionOptions.options.length;
    while (addressSelectionOptions.options.length > 1){
      addressSelectionOptions.remove(1);
    }
    //var firstChoice = document.createElement("option");
    //firstChoice.value = "select an address"
    //firstChoice.text = "select an address"
    // var currenciesC = !{JSON.stringify(currencies)};
    currenciesC.forEach(function(row){
      if (row.currency === currencySelected){
        var choice = document.createElement("option");
        choice.text = row.address;
        choice.value = row.address;
        addressSelectionOptions.appendChild(choice);
      }
    });
  }