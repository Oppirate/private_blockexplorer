$(document).ready(function(){
    var setCurrencies = new Set();
    $("#currency option:gt(0)").toArray().forEach(function(op){
        if (setCurrencies.has(op.value)){
            op.remove();
        } else {
            setCurrencies.add(op.value);
        }
    });

    $("#addresses").on("change", function(event){

        getCurrencyLinks = function(curVal){
            if (curVal === "BTC"){
                var blockLink = "https://blockexplorer.com/block-index/{0}";
                var txLink = "https://blockexplorer.com/tx/{0}";
            } else if (curVal === "BCH"){
                var blockLink = "https://bitcoincash.blockexplorer.com/block-index/{0}";
                var txLink = "https://bitcoincash.blockexplorer.com/tx/{0}";
            } else if (curVal === "ZEC"){
                var blockLink = "https://zcash.blockexplorer.com/block-index/{0}";
                var txLink = "https://zcash.blockexplorer.com/tx/{0}";
            } else if (curVal === "DASH"){
                var blockLink = "https://bchain.info/DASH/block/{0}";
                var txLink = "https://bchain.info/DASH/tx/{0}";
            } else if (curVal === "ETH"){
                var blockLink = "https://etherscan.io/block/{0}";
                var txLink = "https://etherscan.io/tx/{0}";
            } else if (curVal === "ETC"){
                var blockLink = "http://gastracker.io/block/{0}";
                var txLink = "http://gastracker.io/tx/{0}";
            } else {
                throw "undefined currency";
            }
            return [blockLink, txLink];
        }

        var address = event.target.value;
        var currencyValue = $("#currency")[0].value;
        var transactionsList = $("#transactions > tbody")
        $(transactionsList).empty();
        console.log(address);
        $.get("transactions", {currency: currencyValue, address: address}, 
                function(data, status, headers, config){
                        var balance = 0;
                        var result = JSON.parse(data)
                        console.log(result);
                        if (result.length !== 0){
                            balance = result[0].balance;
                        }
                        $("p[name=balance]").html(balance);
                        var temp = getCurrencyLinks(currencyValue),
                            blockLink = temp[0],
                            txLink = temp[1];
                        result.forEach(element => {
                            var currentTxTd = '<a href="' + txLink.replace("{0}", element.tx_hash) + '" target="_blank">' + element.tx_hash + "</a>";
                            var currentBlockTd = '<a href="' + blockLink.replace("{0}", element.block_height) + '" target="_blank">' + element.block_height + "</a>";                           
                            var datetimeString = element.time.replace("T", " ");
                            $("<tr><td>" + datetimeString + '</td><td>' + currentBlockTd+ "</td><td>" + currentTxTd+ "</td><td>"+ element.value +"</td></tr>").appendTo(transactionsList);                            
                        });
                    });
    });
});