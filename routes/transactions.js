var express = require('express');
var auth = require('../middlewares/auth');
var router = express.Router();
var blockchainTransactions = require('../models/blockchainTransaction')
/* GET transactions. */
router.get('/', auth, function(req, res, next) {
    var address = req.param('address');
    var currency = req.param('currency');
    console.log(currency + ' ' + address);
    blockchainTransactions.getTransactions(currency, address, function(result){
        result.sort(function(a,b){
            return b.time.getTime() - a.time.getTime();
        });
        res.send(JSON.stringify(result));
    });
});

module.exports = router;