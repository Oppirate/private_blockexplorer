var express = require('express');
var auth = require('../middlewares/auth');
var router = express.Router();
var blockchainTransactions = require('../models/blockchainTransaction')

/* GET home page. */
router.get('/', auth, function(req, res, next) {
  // blockchainTransactions.getAddressesForCurrencies(function(result){
  //   res.render('index', { title: 'Express', currencies: result});
  // });
  rows = [
    {currency: 'BTC', address: '3G2zkVDVJLWymX9HM5ja5sLzEwUETddWdt'}, 
    {currency: 'BTC', address: '12yGv8jG6sMojrLtC4FTDBW969m93ovA1E'}, 
    {currency: 'ETC', address: '0xA51aA38D643b2BF6875bfc2e9369578799275A24'}, 
    {currency: 'ETC', address: '0x6190ad253F0C9498Ff156A96db7289a60Ec85f24'},
    {currency: 'ETH', address: '0x3Ff39a7553F0D516Ab1418db6647946292D6E4f5'},
    {currency: 'ETH', address: '0x5160bf5CcEa98503534E992bFca42061051cf70A'}, 
    {currency: 'ZEC', address: 't1c1GCTZXXJ7Eow4nbVEjKoJ6XjcAitt2FV'},
    {currency: 'ZEC', address: 't1fgPfzu78ZaSRMhMX2XrQUsQeL1r1HHu7n'},
    {currency: 'DASH', address: 'Xu7pxcZ49jNG1PsDUcy5fEyTSCAchREbpf'}, 
    {currency: 'BCH', address: '17aQkRBdnEVLC5N9uAeN3d6dVYyjhLqHhx'}, 
    {currency: 'BCH', address: '12yGv8jG6sMojrLtC4FTDBW969m93ovA1E'}
  ]

  res.render('index', { title: 'BlockchainExplorer', currencies: rows});
});

module.exports = router;
