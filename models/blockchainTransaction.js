const Sequalize = require('sequelize');
const Op = Sequalize.Op;

const orm = new Sequalize('coins_data', 'postgres', 'ovokuicvepdukfufwhuyquikaracleg', {
  dialect: 'postgres',
  host: 'localhost',
  port: '5432'
});

orm.authenticate().then(() => {
  console.log("connection established !");
}).catch(err => {
  console.log("Unable to connect to the database", err);
});

const BlockchainTransaction = orm.define('blockchain_transaction', {
  tx_hash: {type: Sequalize.STRING, allowNull: false, primaryKey:true},
  currency: {type: Sequalize.STRING, allowNull: false, index: true},
  address: {type: Sequalize.STRING, allowNull: false, index: true},
  time: {type: Sequalize.DATE, allowNull: false},
  value: {type: Sequalize.DOUBLE, allowNull: false},
  block_height: {type: Sequalize.INTEGER, allowNull: false},
  confirmations: {type: Sequalize.INTEGER, allowNull: false},
  balance: {type: Sequalize.DOUBLE, allowNull: false}
}, {
  freezeTableName: true,
  timestamps: false
});

exports.getTransactions = function(currency, address, callback){
  BlockchainTransaction.findAll({
    where: {
      currency: {[Op.eq]: currency},
      address: {[Op.eq]: address}
    }
  }).then(result => {
    //result.rows.forEach(function(row){
    //  console.log(row.dataValues);
    //});
    //console.log(result.rows);
    callback(result);
  });
}

exports.getAddressesForCurrencies = function(callback){
  BlockchainTransaction.findAll({
    attributes: ['currency', 'address'],
    group: ['currency', 'address']
  }).then(result => {
    callback(result);
  });
}

const logRows = function(rows){
  rows.forEach(function(row){
    console.log(row.dataValues);
  })
};
// combinations = getAddressesForCurrencies(logRows);
// transactions = getTransactions('ETH', "0x3Ff39a7553F0D516Ab1418db6647946292D6E4f5", logRows);
